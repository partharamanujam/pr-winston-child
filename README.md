# pr-winston-child

Winston logger with a child

## Philosophy

Winston logger with inheritable/overridable context

## Installation

```bash
$ npm install pr-winston-child
```

## APIs

This is an extension of [winston](https://www.npmjs.com/package/winston) module, which can be used in place of it. In fact, this module uses the base winston module, and adds a winston.ChildLogger class. 

The winston.ChildLogger is same as winston.Logger class with added context which is a JSON-object with key-value pairs. It has the following methods (in addition to those inherited from the base winston.Logger class):
* constructor: new winston.ChildLogger(winston-options, context)
* method: child(context)

Everytime a child is created with a new context, the child shares all attributes of the parent logger, and also inherits context from its parent and overrdies any which may be conflicting.

## Usage

```js
var winston = require("pr-winston-child");

var father, son, daughter, grandson;

father = new winston.ChildLogger(winstonOptions, {
    "gen": "1st",
    "first": "Noname",
    "last": "Doe",
    "sex": "male"
});

son = father.child({
    "gen": "2nd",
    "first": "John",
    "sex": "male"
});

daughter = father.child({
    "gen": "2nd",
    "first": "Jane",
    "sex": "female"
});

grandson = son.child({
    "gen": "3rd",
    "first": "Jonathan",
    "sex": "male"
});

father.info("father");
son.info("son");
daughter.info("daughter");
grandson.info("grandson");
```

Should output:  
> info: father gen=1st, first=Noname, last=Doe, sex=male  
> info: son gen=2nd, first=John, last=Doe, sex=male  
> info: daughter gen=2nd, first=Jane, last=Doe, sex=female  
> info: grandson gen=3rd, first=Jonathan, last=Doe, sex=male  

Refer to [example.js](https://gitlab.com/partharamanujam/pr-winston-child/blob/master/test/example.js) implementation for a sample.

## Test

```bash
$ npm install # inside pr-winston-child
$ npm test
```
