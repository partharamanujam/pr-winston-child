"use strict";

var assert = require("assert"),
    winston = require("../index"),
    WinstonSpy = require("winston-spy"),
    td = require("testdouble");

/* globals describe, it */
describe("testing winston-child", function testWinstonChild() {
    var logger, parent,
        spy = td.function(".spy"),
        transports = [new WinstonSpy({
            "spy": spy
        })];

    it("winston.Logger default", function defaultLogger(done) {
        logger = new winston.Logger({
            "transports": transports
        });
        logger.info("test", {
            "hello": "world"
        });
        assert.doesNotThrow(function test() {
            td.verify(spy("info", "test", {
                "hello": "world"
            }));
        });
        done();
    });
    it("winston.ChildLogger default", function childLogger(done) {
        parent = logger = new winston.ChildLogger({
            "transports": transports
        });
        logger.info("test", {
            "hello": "world"
        });
        assert.doesNotThrow(function test() {
            td.verify(spy("info", "test", {
                "hello": "world"
            }));
        });
        done();
    });
    it("winston.ChildLogger child", function defaultLogging(done) {
        logger = logger.child({
            "attr1": "value1"
        });

        logger.info("test", {
            "hello": "world"
        });
        assert.doesNotThrow(function test() {
            td.verify(spy("info", "test", {
                "hello": "world",
                "attr1": "value1"
            }));
        });
        done();
    });
    it("winston.ChildLogger grand-child", function defaultLogging(done) {
        logger = logger.child({
            "attr2": "value2"
        });
        logger.info("test", {
            "hello": "world"
        });
        assert.doesNotThrow(function test() {
            td.verify(spy("info", "test", {
                "hello": "world",
                "attr1": "value1",
                "attr2": "value2"
            }));
        });
        done();
    });
    it("winston.ChildLogger parent-intact", function defaultLogging(done) {
        parent.info("test", {
            "hello": "world"
        });
        assert.doesNotThrow(function test() {
            td.verify(spy("info", "test", {
                "hello": "world"
            }));
        });
        done();
    });
});