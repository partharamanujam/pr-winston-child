"use strict";

var winston = require("../index"); // pr-winston-child

var father, son, daughter, grandson,
    winstonOptions = {
        "transports": [new winston.transports.Console()]
    };

father = new winston.ChildLogger(winstonOptions, {
    "gen": "1st",
    "first": "Noname",
    "last": "Doe",
    "sex": "male"
});

son = father.child({
    "gen": "2nd",
    "first": "John",
    "sex": "male"
});

daughter = father.child({
    "gen": "2nd",
    "first": "Jane",
    "sex": "female"
});

grandson = son.child({
    "gen": "3rd",
    "first": "Jonathan",
    "sex": "male"
});

father.info("father");
son.info("son");
daughter.info("daughter");
grandson.info("grandson");

/*
info: father gen=1st, first=Noname, last=Doe, sex=male
info: son gen=2nd, first=John, last=Doe, sex=male
info: daughter gen=2nd, first=Jane, last=Doe, sex=female
info: grandson gen=3rd, first=Jonathan, last=Doe, sex=male
*/