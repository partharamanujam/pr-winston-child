"use strict";

var winston = require("winston");

class ChildLogger extends winston.Logger {
    constructor(options, context) {
        var that;

        super(options);
        that = this;
        this.winstonOptions = options;
        this.childContext = context || {};
        this.rewriters.push(function updateMeta(level, msg, meta) {
            return Object.assign({}, meta, that.childContext);
        });
    }

    child(context) {
        return new ChildLogger(this.winstonOptions,
            Object.assign({}, this.childContext, context));
    }
}

winston.ChildLogger = ChildLogger;

// exports
module.exports = winston;